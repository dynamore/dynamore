<?php

declare(strict_types=1);

namespace Dynamore;

/**
 * @phpstan-type TableDefinition array{
 *  TableName: string,
 *  AttributeDefinitions: list<array{AttributeName: string, AttributeType: string}>,
 *  BillingMode?: string,
 *  DeletionProtectionEnabled?: bool,
 *  StreamSpecification?: array{StreamViewType: string, StreamEnabled?: bool},
 *  GlobalSecondaryIndexes?: list<array{
 *     IndexName: string,
 *     KeySchema: list<array{AttributeName: string, KeyType: string}>,
 *     Projection: array{ProjectionType: string, NonKeyAttributes?: list<string>},
 *     ProvisionedThroughput: array{ReadCapacityUnits: int, WriteCapacityUnits: int}
 *  }>,
 *  KeySchema: list<array{AttributeName: string, KeyType: string}>,
 *  LocalSecondaryIndexes?: list<array{
 *     IndexName: string,
 *     KeySchema: list<array{AttributeName: string, KeyType: string}>,
 *     Projection: array{ProjectionType: string, NonKeyAttributes?: list<string>}
 *  }>,
 *  ProvisionedThroughput?: array{ReadCapacityUnits: int, WriteCapacityUnits: int},
 *  SSESpecification?: array{Enabled: bool, SSEType: string, KMSMasterKeyId?: string},
 *  StreamSpecification?: array{StreamEnabled: bool, StreamViewType: string},
 *  TableClass?: string,
 *  Tags?: list<array{Key: string, Value: string}>
 * }
 * @phpstan-type ResourceDefinition array{Type: string, Properties: TableDefinition}
 */
readonly class TableDefinitionParser
{
    public function __construct(
        private bool $useResourceKeyAsTableName = false,
    ) {
    }

    /**
     * @param array<string|int, ResourceDefinition> $resources
     * @return array<int, TableDefinition>
     */
    public function parse(array $resources): array
    {
        /** @var array<int, string|int> $resourceKeys */
        $resourceKeys = array_keys($resources);

        return array_reduce(
            array: $resourceKeys,
            callback: function (array $definitions, string|int $resourceKey) use ($resources): array {
                $resource = $resources[$resourceKey];

                if ($resource['Type'] !== 'AWS::DynamoDB::Table') {
                    return $definitions;
                }

                /** @var TableDefinition $definition */
                $definition = $resource['Properties'];

                // It's common for table names to be generated dynamically, to include the stage suffix for
                // example. This makes it harder to read the Cloudformation/Serverless template, so we'll
                // allow the developer to use the resource key as the table name instead.
                if ($this->useResourceKeyAsTableName) {
                    if (is_int($resourceKey)) {
                        throw new \RuntimeException(
                            'Resource indexes must be of type string if useResourceKeyAsTableName is true.'
                        );
                    }

                    $definition['TableName'] = $resourceKey;
                }

                // Cloudformation only requires the StreamViewType under StreamSpecification.
                // The DynamoDB API requires the StreamEnabled flag to be set to true.
                if (isset($definition['StreamSpecification'])) {
                    $definition['StreamSpecification']['StreamEnabled'] = true;
                }

                $definitions[] = $definition;

                return $definitions;
            },
            initial: []
        );
    }
}
