<?php

declare(strict_types=1);

namespace Dynamore;

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
use Symfony\Component\Yaml\Yaml;

/**
 * @phpstan-import-type ResourceDefinition from TableDefinitionParser
 * @phpstan-import-type TableDefinition from TableDefinitionParser
 */
class DynamoreBuilder
{
    private string $resourcesPath = '';
    private bool $useResourceKeyAsTableName = false;

    public function __construct(
        private readonly DynamoDbClient $client,
        private readonly Marshaler $marshaler = new Marshaler(),
    ) {
    }

    public function withResourcesPath(string $resourcesPath): self
    {
        $this->resourcesPath = $resourcesPath;

        return $this;
    }

    public function useResourceKeyAsTableName(): self
    {
        $this->useResourceKeyAsTableName = true;

        return $this;
    }

    public function buildFromTemplate(string $filePath): Dynamore
    {
        // get file extension
        $extension = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
        $contents = file_get_contents($filePath);

        if ($contents === false) {
            throw new \RuntimeException('Failed to read DynamoDB table definitions from YAML');
        }

        $data = match ($extension) {
            'json' => json_decode($contents, true, 512, JSON_THROW_ON_ERROR),
            'yaml', 'yml' => Yaml::parse($contents, Yaml::PARSE_CUSTOM_TAGS),
            default => throw new \InvalidArgumentException('Unsupported file extension')
        };

        if (!is_array($data)) {
            throw new \LogicException('Expected array of table definitions');
        }

        return $this->buildFromArray($data);
    }

    /** @param ResourceDefinition[] $data */
    public function buildFromArray(array $data): Dynamore
    {
        $resourceDefinitions = $this->findResourceDefinitions($data, $this->resourcesPath);
        $parser = new TableDefinitionParser($this->useResourceKeyAsTableName);

        $tableDefinitions = $parser->parse($resourceDefinitions);

        return new Dynamore(
            client: $this->client,
            marshaler: $this->marshaler,
            tableDefinitions: $tableDefinitions
        );
    }

    /**
     * @param ResourceDefinition[] $data
     * @return ResourceDefinition[]
     */
    private function findResourceDefinitions(array $data, string $resourcesPath): array
    {
        if (empty($resourcesPath)) {
            return $data;
        }

        $indexes = explode('.', $resourcesPath);

        foreach ($indexes as $index) {
            if (array_key_exists($index, $data) === false) {
                throw new \InvalidArgumentException(
                    sprintf('Resource definitions not found at path "%s"', $resourcesPath)
                );
            }

            /** @var ResourceDefinition[] $data */
            $data = $data[$index];
        }

        return $data;
    }
}
