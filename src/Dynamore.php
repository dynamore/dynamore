<?php

declare(strict_types=1);

namespace Dynamore;

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

/**
 * @phpstan-import-type TableDefinition from TableDefinitionParser
 */
readonly class Dynamore
{
    /**
     * @param TableDefinition[] $tableDefinitions
     */
    public function __construct(
        public DynamoDbClient $client,
        public Marshaler $marshaler,
        public array $tableDefinitions,
    ) {
    }

    public function createTables(): void
    {
        if (empty($this->tableDefinitions)) {
            return;
        }

        $existingTables = $this->listTableNames();
        $tablesToCreate = array_map(
            callback: static fn(array $tableDefinition) => $tableDefinition['TableName'],
            array: $this->tableDefinitions
        );

        $alreadyExisting = array_intersect($existingTables, $tablesToCreate);

        if (!empty($alreadyExisting)) {
            throw new \RuntimeException(sprintf(
                'Cannot create DynamoDB tables. The following tables already exist: %s',
                implode(', ', $alreadyExisting)
            ));
        }

        foreach ($this->tableDefinitions as $tableDefinition) {
            $this->client->createTable($tableDefinition);
        }
    }

    public function deleteTables(): void
    {
        $existingTables = $this->listTableNames();
        $tablesToDelete = array_map(
            callback: static fn(array $tableDefinition) => $tableDefinition['TableName'],
            array: $this->tableDefinitions
        );

        $targetTables = array_intersect($existingTables, $tablesToDelete);

        foreach ($targetTables as $targetTable) {
            try {
                $this->client->deleteTable(['TableName' => $targetTable]);
            } catch (DynamoDbException $e) {
                if ($e->getStatusCode() !== 400) {
                    throw $e;
                }
            }
        }
    }

    /** @return string[] */
    public function listTableNames(): array
    {
        $result = $this->client->listTables();
        /** @var string[] $tableNames */
        $tableNames = $result->get('TableNames') ?? [];

        return $tableNames;
    }

    public function resetTables(): self
    {
        $this->deleteTables();
        $this->createTables();

        return $this;
    }

    /** @param array<string, mixed> $item */
    public function putItem(string $table, array $item): void
    {
        $this->client->putItem([
            'TableName' => $table,
            'Item' => $this->marshaler->marshalItem($item),
        ]);
    }

    /**
     * @param array<string, scalar> $keys
     * @return array<string, mixed>|null
     */
    public function getItem(string $table, array $keys): ?array
    {
        /** @var array{'Item'?: array<string, mixed>} $result */
        $result = $this->client->getItem([
            'TableName' => $table,
            'Key' => $this->marshaler->marshalItem($keys),
        ]);

        if (isset($result['Item'])) {
            /** @var array<string, mixed> $item */
            $item = $this->marshaler->unmarshalItem($result['Item']);

            return $item;
        }

        return null;
    }
}
