# Dynamore - DynamoDB Local Integration Testing for PHP

Dynamore is a PHP package designed to simplify the process of creating DynamoDB tables for local integration testing.
It streamlines the interaction with a local "dynamodb-local" container, allowing developers to easily set up DynamoDB
tables based on predefined schemas declared in JSON or YAML files using CloudFormation notation.

## Features

- Create DynamoDB tables against a local "dynamodb-local" container for integration testing.
- Take advantage of already defined table schemas in JSON or YAML (like a serverless.yaml file).
- Parse schema files and use the AWS PHP SDK to interact with the local DynamoDB instance.
- Simplify the testing process by providing an easy-to-use API for table creation.

## Installation

Install Dynamore using [Composer](https://getcomposer.org/):

```bash
composer require dynamore/dynamore --dev
```

## Usage
Considering you have a traditional serverless.yaml file with a DynamoDB table defined like this:

```yaml
service: my-service

provider:
  name: aws
  runtime: nodejs14.x
  region: us-east-1

resources:
  Resources:
    MyDynamoDBTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        TableName: MyTable
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
```

You can use Dynamore on your tests to recreate the table in a local DynamoDB instance:

```php
<?php

declare(strict_types=1);

namespace Tests;

use Aws\DynamoDb\DynamoDbClient;
use Dynamore\DynamoreBuilder;
use PHPUnit\Framework\TestCase;

class DynamoreTestCase extends TestCase
{
    protected Dynamore $dynamore;

    protected function setUp(): void
    {
        parent::setUp();

        $dynamoDbClient = new DynamoDbClient([
            'region' => getenv('AWS_REGION'),
            'version' => 'latest',
            'endpoint' => getenv('DYNAMODB_ENDPOINT') ?: 'http://localhost:8000',
            'credentials' => [
                'key' => getenv('AWS_ACCESS_KEY_ID') ?: 'notARealKey',
                'secret' => getenv('AWS_SECRET_ACCESS_KEY') ?: 'notARealSecret',
            ],
        ]);
       
        $this->dynamore = (new DynamoreBuilder($dynamoDbClient))
            ->withResourcesPath('resources.Resources')
            ->useResourceKeyAsTableName()
            ->buildFromTemplate(__DIR__ . '../serverless.yaml');
            
        $this->dynamore->resetTables();
    }
}
```

You can see we use the `resetTables` method to recreate the tables before each test. This method will delete all tables
and recreate them using the schema defined in the serverless.yaml file, making sure no state is left behind between
tests.

## Note
While Dynamore simplifies the testing process, it may not cover all the options available in a typical CloudFormation
template. Therefore, the created tables may not match exactly in specifications and Dyanmore should only be used for
local integration testing.

## Contributing
Feel free to contribute, report issues, or make suggestions.

## License
Dynamore is licensed under the MIT License - see the LICENSE file for details.

