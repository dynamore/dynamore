<?php

declare(strict_types=1);

namespace DynamoreTest;

use Dynamore\TableDefinitionParser;
use DynamoreTest\Concern\InteractsWithDefinitions;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class TableDefinitionParserTest extends TestCase
{
    use InteractsWithDefinitions;

    #[Test]
    public function it_can_parse_resource_definitions_as_is(): void
    {
        $parser = new TableDefinitionParser(useResourceKeyAsTableName: false);
        $resources = $this->getDefaultResourceDefinitions();

        $tableDefinitions = $parser->parse($resources);

        $expected = array_values($this->getDefaultTableDefinitions());

        $this->assertEquals($expected, $tableDefinitions);
    }

    #[Test]
    public function it_can_parse_resource_definitions_as_using_resource_key_as_table_name(): void
    {
        $parser = new TableDefinitionParser(useResourceKeyAsTableName: true);

        $resources = $this->getDefaultResourceDefinitions();

        $tableDefinitions = $parser->parse($resources);

        $expected = array_values($this->getDefaultTableDefinitions());
        $expected[0]['TableName'] = 'myResourceKey';

        $this->assertEquals($expected, $tableDefinitions);
    }

    #[Test]
    public function it_expands_the_stream_specification_attribute_if_present(): void
    {
        $parser = new TableDefinitionParser(useResourceKeyAsTableName: false);

        $resources = $this->getDefaultResourceDefinitions();
        $resources['myResourceKey']['Properties']['StreamSpecification'] = [
            'StreamViewType' => 'NEW_AND_OLD_IMAGES',
        ];

        $tableDefinitions = $parser->parse($resources);

        $this->assertEquals(
            ['StreamEnabled' => true, 'StreamViewType' => 'NEW_AND_OLD_IMAGES'],
            $tableDefinitions[0]['StreamSpecification'] ?? null,
        );
    }
}
