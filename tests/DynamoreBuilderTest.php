<?php

declare(strict_types=1);

namespace DynamoreTest;

use Dynamore\DynamoreBuilder;
use DynamoreTest\Concern\InteractsWithDefinitions;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class DynamoreBuilderTest extends DynamoDbTestCase
{
    use InteractsWithDefinitions;

    private DynamoreBuilder $builder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builder = new DynamoreBuilder($this->dynamoDbClient);
    }

    #[Test]
    #[DataProvider('fileDataProvider')]
    public function it_builds_dynamore_from_supported_formats(string $filePath, string $resourcesPath): void
    {
        $dynamo = $this->builder->withResourcesPath($resourcesPath)->buildFromTemplate($filePath);
        $dynamo->createTables();

        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals(['myTableName'], $result->get('TableNames'));
    }

    #[Test]
    #[DataProvider('fileDataProvider')]
    public function it_builds_dynamore_using_resource_keys_as_table_names(string $filePath, string $resourcesPath): void
    {
        $dynamo = $this->builder
            ->withResourcesPath($resourcesPath)
            ->useResourceKeyAsTableName()
            ->buildFromTemplate($filePath);
        $dynamo->createTables();

        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals(['myDynamoDBTable'], $result->get('TableNames'));
    }

    #[Test]
    public function it_throws_if_resource_path_is_not_valid(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Resource definitions not found at path "invalid-path"');

        $this->builder
            ->withResourcesPath('invalid-path')
            ->buildFromTemplate(__DIR__ . '/Fixture/cloudformation-format.yaml');
    }

    #[Test]
    public function it_can_follow_dot_separated_paths(): void
    {
        $dynamore = $this->builder
            ->withResourcesPath('these.are.really.nested.resources')
            ->buildFromTemplate(__DIR__ . '/Fixture/nested-resource-cloudformation-format.yaml');
        $dynamore->createTables();

        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals(['myTableName'], $result->get('TableNames'));
    }

    /** @return array<string, array<int, string>> */
    public static function fileDataProvider(): iterable
    {
        yield 'YAML' => [__DIR__ . '/Fixture/cloudformation-format.yaml', 'Resources'];
        yield 'JSON' => [__DIR__ . '/Fixture/cloudformation-format.json', 'Resources'];
    }
}
