<?php

declare(strict_types=1);

namespace DynamoreTest;

use Dynamore\Dynamore;
use DynamoreTest\Concern\InteractsWithDefinitions;
use Aws\DynamoDb\Marshaler;
use PHPUnit\Framework\Attributes\Test;

class DynamoreTest extends DynamoDbTestCase
{
    use InteractsWithDefinitions;

    private Dynamore $dynamoLocal;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dynamoLocal = new Dynamore(
            client: $this->dynamoDbClient,
            marshaler: new Marshaler(),
            tableDefinitions: $this->getDefaultTableDefinitions(),
        );
    }

    #[Test]
    public function it_can_creates_and_deletes_tables(): void
    {
        $this->dynamoLocal->createTables();
        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals(['myDynamoDBTable'], $result->get('TableNames'));

        $this->dynamoLocal->deleteTables();
        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals([], $result->get('TableNames'));
    }

    #[Test]
    public function it_can_reset_tables(): void
    {
        $this->dynamoLocal->createTables();
        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals(['myDynamoDBTable'], $result->get('TableNames'));

        // If we call createTables again, it will throw an exception from DynamoDB, because the table already exists
        try {
            $this->dynamoLocal->createTables();
        } catch (\RuntimeException $e) {
            $this->assertEquals(
                'Cannot create DynamoDB tables. The following tables already exist: myDynamoDBTable',
                $e->getMessage()
            );
        }

        // If we call resetTables, it will delete the table and create it again
        $this->dynamoLocal->resetTables();
        $result = $this->dynamoDbClient->listTables();
        $this->assertEquals(['myDynamoDBTable'], $result->get('TableNames'));
    }

    #[Test]
    public function it_can_put_item_on_table(): void
    {
        $this->dynamoLocal->resetTables();

        $this->dynamoLocal->putItem(
            table: 'myDynamoDBTable',
            item: [
                'Album' => 'Songs About Life',
                'Artist' => 'Acme Band',
            ],
        );

        $result = $this->dynamoDbClient->getItem([
            'TableName' => 'myDynamoDBTable',
            'Key' => [
                'Album' => [
                    'S' => 'Songs About Life',
                ],
                'Artist' => [
                    'S' => 'Acme Band',
                ],
            ],
        ]);
        /** @var array{Album: array{S: string}, Artist: array{S: string}} $item */
        $item = $result->get('Item');

        $this->assertEquals('Songs About Life', $item['Album']['S']);
        $this->assertEquals('Acme Band', $item['Artist']['S']);
    }

    #[Test]
    public function it_can_get_item_from_table(): void
    {
        $this->dynamoLocal->resetTables();

        $this->dynamoDbClient->putItem([
            'TableName' => 'myDynamoDBTable',
            'Item' => [
                'Album' => [
                    'S' => 'Songs About Life',
                ],
                'Artist' => [
                    'S' => 'Acme Band',
                ],
            ],
        ]);

        /** @var array{Album: string, Artist: string} $item */
        $item = $this->dynamoLocal->getItem(
            table: 'myDynamoDBTable',
            keys: [
                'Album' => 'Songs About Life',
                'Artist' => 'Acme Band',
            ],
        );

        $this->assertEquals('Songs About Life', $item['Album']);
        $this->assertEquals('Acme Band', $item['Artist']);
    }
}
