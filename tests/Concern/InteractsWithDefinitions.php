<?php

declare(strict_types=1);

namespace DynamoreTest\Concern;

use Dynamore\TableDefinitionParser;

/**
 * @phpstan-import-type ResourceDefinition from TableDefinitionParser
 * @phpstan-import-type TableDefinition from TableDefinitionParser
 */
trait InteractsWithDefinitions
{
    /**
     * @return array<string|int, TableDefinition>
     */
    public function getDefaultTableDefinitions(): array
    {
        return [
            'myResourceKey' => [
                'TableName' => 'myDynamoDBTable',
                'AttributeDefinitions' => [
                    [
                        'AttributeName' => 'Album',
                        'AttributeType' => 'S',
                    ],
                    [
                        'AttributeName' => 'Artist',
                        'AttributeType' => 'S',
                    ],
                    [
                        'AttributeName' => 'Sales',
                        'AttributeType' => 'N',
                    ],
                    [
                        'AttributeName' => 'NumberOfSongs',
                        'AttributeType' => 'N',
                    ],
                ],
                'KeySchema' => [
                    [
                        'AttributeName' => 'Album',
                        'KeyType' => 'HASH',
                    ],
                    [
                        'AttributeName' => 'Artist',
                        'KeyType' => 'RANGE',
                    ],
                ],
                'ProvisionedThroughput' => [
                    'ReadCapacityUnits' => 5,
                    'WriteCapacityUnits' => 5,
                ],
                'GlobalSecondaryIndexes' => [
                    [
                        'IndexName' => 'myGSI',
                        'KeySchema' => [
                            [
                                'AttributeName' => 'Sales',
                                'KeyType' => 'HASH',
                            ],
                            [
                                'AttributeName' => 'Artist',
                                'KeyType' => 'RANGE',
                            ],
                        ],
                        'Projection' => [
                            'NonKeyAttributes' => [
                                'Album',
                                'NumberOfSongs',
                            ],
                            'ProjectionType' => 'INCLUDE',
                        ],
                        'ProvisionedThroughput' => [
                            'ReadCapacityUnits' => 5,
                            'WriteCapacityUnits' => 5,
                        ],
                    ],
                    [
                        'IndexName' => 'myGSI2',
                        'KeySchema' => [
                            [
                                'AttributeName' => 'NumberOfSongs',
                                'KeyType' => 'HASH',
                            ],
                            [
                                'AttributeName' => 'Sales',
                                'KeyType' => 'RANGE',
                            ],
                        ],
                        'Projection' => [
                            'NonKeyAttributes' => [
                                'Album',
                                'Artist',
                            ],
                            'ProjectionType' => 'INCLUDE',
                        ],
                        'ProvisionedThroughput' => [
                            'ReadCapacityUnits' => 5,
                            'WriteCapacityUnits' => 5,
                        ],
                    ],
                ],
                'LocalSecondaryIndexes' => [
                    [
                        'IndexName' => 'myLSI',
                        'KeySchema' => [
                            [
                                'AttributeName' => 'Album',
                                'KeyType' => 'HASH',
                            ],
                            [
                                'AttributeName' => 'Sales',
                                'KeyType' => 'RANGE',
                            ],
                        ],
                        'Projection' => [
                            'NonKeyAttributes' => [
                                'Artist',
                                'NumberOfSongs',
                            ],
                            'ProjectionType' => 'INCLUDE',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array<string|int, ResourceDefinition>
     */
    public function getDefaultResourceDefinitions(): array
    {
        $tableDefinitions = $this->getDefaultTableDefinitions();
        $resourceKeys = array_keys($tableDefinitions);

        return array_reduce(
            array: $resourceKeys,
            callback: static function (array $resources, string|int $key) use ($tableDefinitions): array {
                $resources[$key] = ['Type' => 'AWS::DynamoDB::Table', 'Properties' => $tableDefinitions[$key]];

                return $resources;
            },
            initial: []
        );
    }
}
