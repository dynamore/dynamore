<?php

declare(strict_types=1);

namespace DynamoreTest;

use Aws\DynamoDb\DynamoDbClient;
use PHPUnit\Framework\TestCase;

class DynamoDbTestCase extends TestCase
{
    protected DynamoDbClient $dynamoDbClient;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dynamoDbClient = new DynamoDbClient([
            'region' => getenv('AWS_REGION'),
            'version' => 'latest',
            'endpoint' => getenv('DYNAMODB_ENDPOINT') ?: 'http://localhost:8000',
            'credentials' => [
                'key' => getenv('AWS_ACCESS_KEY_ID') ?: 'notARealKey',
                'secret' => getenv('AWS_SECRET_ACCESS_KEY') ?: 'notARealSecret',
            ],
        ]);
    }

    protected function tearDown(): void
    {
        $result = $this->dynamoDbClient->listTables();

        /** @var string[] $tables */
        $tables = $result->get('TableNames');
        foreach ($tables as $table) {
            $this->dynamoDbClient->deleteTable([
                'TableName' => $table,
            ]);
        }

        parent::tearDown();
    }
}
